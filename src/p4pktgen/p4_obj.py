# Added support
from __future__ import print_function

"""p4_obj.py: Base p4 object"""

__author__ = "Colin Burgin"
__copyright__ = "Copyright 2017, Virginia Tech"
__credits__ = [""]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = ""
__email__ = "cburgin@vt.edu"
__status__ = "in progress"

# Standard Python Libraries

# Installed Packages/Libraries

# P4 Specfic Libraries

# Local API Libraries

class P4_Obj(object):
	"""Base P4 object to be used by other things"""
pass